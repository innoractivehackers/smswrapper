# SMS-Wrapper ( Laravel 5 Package )

## Installation

Just add

    "repositories": [
      {
        "type": "vcs",
        "url":  "git@bitbucket.org:innoractivehackers/smswrapper.git"
      }
    ],

to your composer.json. Then run the `composer require` command from your terminal:

    composer require innoractive/sms-wrapper:dev-master

Then in your `config/app.php` add
```php
    Innoractive\SMSWrapper\SMSWrapperProvider::class,
```
in the `providers` array.

Then run:

    php artisan vendor:publish
    
## Config

config/sms-wrapper.php

Supported SMS Service Provider

    'mexcomm', 'nexmo', 'trio', 'firemobile', 'macrokiosk', 'elfo'
    
## Changes

**version 1.3**

Elfo supported

**version 1.2**

MACROKIOSK supported
    
**version 1.1**

FireMobile supported

**version 1.0**

Mexcomm, Nexmo, Trio supported
    
## TODO

- Check mobile number format valid before send