<?php

/**
 * core for sms
 */

namespace Innoractive\SMSWrapper;

use Innoractive\SMSWrapper\Classes\Provider\Elfo;
use Innoractive\SMSWrapper\Classes\Provider\FireMobile;
use Innoractive\SMSWrapper\Classes\Provider\MacroKiosk;
use Innoractive\SMSWrapper\Classes\Provider\Mexcomm;
use Innoractive\SMSWrapper\Classes\Provider\Nexmo;
use Innoractive\SMSWrapper\Classes\Provider\Provider;
use Innoractive\SMSWrapper\Classes\Provider\Trio;
use Innoractive\SMSWrapper\Classes\Response;

class SMSCore {
    const VERSION = '1.1';

    protected $providers = [];
    protected $settings = [];

    function __construct($providers, $settings) {
        $this->providers = $providers;
        $this->settings = $settings;
    }

    /**
     * @param $message
     * @param $recipients
     * @param null $from
     * @param int $providerIndex
     * @return Response
     */
    public function send($message, $recipients, $from = null, $providerIndex = 0){
        // get provider
        $provider = $this->getProviderToSend($providerIndex);

        $return = $provider->send($message, $recipients, $from);

        return new Response($provider, $return);
    }

    protected function getProviderIndexToSend($providerIndex){
        // check if $providerIndex > providers length
        if ($providerIndex >= count($this->providers)){
            // get last
            $providerIndex = count($this->providers) - 1;
        }

        return $providerIndex;
    }

    /**
     * @param $providerIndex
     * @return Provider
     */
    protected function getProviderToSend($providerIndex){
        // get provider index to send
        $providerIndex = $this->getProviderIndexToSend($providerIndex);

        // get provider name
        $provider = $this->providers[$providerIndex];

        // get settings
        $settings = $this->getSettings($provider);

        switch ($provider){
            case Mexcomm::NAME:
                return new Mexcomm($settings);
            case Nexmo::NAME:
                return new Nexmo($settings);
            case Trio::NAME:
                return new Trio($settings);
            case FireMobile::NAME:
                return new FireMobile($settings);
            case MacroKiosk::NAME:
                return new MacroKiosk($settings);
            case Elfo::NAME:
                return new Elfo($settings);
            default:
                return null;
        }
    }

    /**
     * @param string $provider
     * @return array
     */
    protected function getSettings($provider){
        return $this->settings[$provider];
    }
}