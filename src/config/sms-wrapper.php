<?php

return [
    // base on priority
    'providers' => ['mexcomm', 'nexmo'],
    'settings' => [
        'mexcomm' => [
            'user' => 'xxxxx',
            'pass' => 'xxxxx',
            'http_enable' => false,
        ],
        'nexmo' => [
            'api_key' => 'xxxxx',
            'api_secret' => 'xxxxx',
            'http_enable' => false,
        ],
        'trio' => [
            'api_key' => 'xxxxx',
            'http_enable' => false,
        ],
        'firemobile' => [
            'user' => 'xxxxx',
            'pass' => 'xxxxx',
            'http_enable' => false,

            // https endpoint, default using http
            // 'endpoint' => 'https://110.4.44.41:15001/cgi-bin/sendsms',
        ],
        'macrokiosk' => [
            'user' => 'xxxxx',
            'pass' => 'xxxxx',
            'http_enable' => false,

            // Default endpoint:
            // 'endpoint' => 'http://www.etracker.cc/bulksms/mesapi.aspx',
        ],
        'elfo' => [
            'api_key' => 'xxxxx',
            'http_enable' => false,

            // Default sender id
            // 'sender_id' => '68889',

            // Default endpoint
            // 'endpoint' => 'https://sms.elfo.com/v1/sms',
        ],
    ],

    // default log setting, can ignore this
    'log' => [
        // logs directory
        'dir' => 'logs',

        // log file name
        'file' => 'sms-wrapper'
    ],
];