<?php

namespace Innoractive\FPXWrapper;

class Helper {
    /**
     *
     * If key not found, return empty string
     *
     * @param $input array
     * @param $key String
     * @return String
     */
    public static function ParseInput($input, $key){
        try {
            $value = $input[$key];
        }catch (\Exception $e){
            $value = '';
        }
        return $value;
    }
}