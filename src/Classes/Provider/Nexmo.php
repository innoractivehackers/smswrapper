<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

class Nexmo extends SingleOnlyProvider{
    const NAME = 'nexmo';

    const SMS_GATEWAY_URL = 'https://rest.nexmo.com/sms/json';
    const DEFAULT_FROM = 'RM0';
    const MODE_SHORTCODE = 'shortcode';

    protected $apiKey;
    protected $apiSecret;

    /**
     * Nexmo constructor.
     * @param $settings
     */
    public function __construct(array $settings){
        parent::__construct($settings);

        $this->apiKey = $settings['api_key'];
        $this->apiSecret = $settings['api_secret'];
    }

    public function getName(){
        return self::NAME;
    }

    function getEndPoint() {
        return self::SMS_GATEWAY_URL;
    }

    protected function getParameter($message, $recipient, $from = null) {
        if (empty($from)){
            $from = self::DEFAULT_FROM;
        }

        return [
            'api_key' => $this->apiKey,
            'api_secret' => $this->apiSecret,
            'to' => $recipient,
            'from' => $from,
            'text' => 'RM0.00 ' . $message
        ];
    }
}