<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

class Trio extends SingleOnlyProvider{
    const NAME = 'trio';

    const SMS_GATEWAY_URL = 'http://cloudsms.trio-mobile.com/index.php/api/bulk_mt';
    const DEFAULT_FROM = 'RM0';
    const MODE_SHORTCODE = 'shortcode';

    const CONTENT_TYPE_ENGLISH = 1;
    const CONTENT_TYPE_UNICODE = 4;

    protected $apiKey;

    public function __construct(array $settings){
        parent::__construct($settings);

        $this->apiKey = $settings['api_key'];
    }

    public function getName(){
        return self::NAME;
    }

    function getEndPoint() {
        return self::SMS_GATEWAY_URL;
    }

    protected function getParameter($message, $recipient, $from = null) {
        // make sure from not empty
        if (empty($from)){
            $from = self::DEFAULT_FROM;
        }

        return [
            'api_key' => $this->apiKey,
            'action' => 'send',
            'to' => $recipient,
            'sender_id' => $from,
            'mode' => self::MODE_SHORTCODE,
            'content_type' => self::CONTENT_TYPE_ENGLISH,
            'msg' => $message
        ];
    }
}