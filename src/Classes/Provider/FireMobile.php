<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

class FireMobile extends SingleOnlyProvider{
    const NAME = 'firemobile';

    const SMS_GATEWAY_URL = 'https://smsapi3.firemobile.my:15001/midwayv3/sendhttpsms.ashx';
    const DEFAULT_FROM = 'RM0';
    const MODE_SHORTCODE = 'shortcode';

    protected $user;
    protected $pass;

    protected $endPoint;

    /**
     * FireMobile constructor.
     * @param $settings
     */
    public function __construct(array $settings){
        parent::__construct($settings);

        $this->user = $settings['user'];
        $this->pass = $settings['pass'];

        // override endpoint
        if (isset($settings['endpoint'])){
            $this->endPoint = $settings['endpoint'];
        }else{
            $this->endPoint = self::SMS_GATEWAY_URL;
        }
    }

    public function getName(){
        return self::NAME;
    }

    function getEndPoint() {
        return $this->endPoint;
    }

    protected function getParameter($message, $recipient, $from = null) {
        if (empty($from)){
            $from = self::DEFAULT_FROM;
        }

        return [
            'Gw-Username' => $this->user,
            'Gw-Password' => $this->pass,
            'Gw-To' => $recipient,
            'Gw-From' => $from,
            'Gw-Coding' => 1,
            'Gw-Text' => 'RM0.00 ' . $message
        ];
    }
}