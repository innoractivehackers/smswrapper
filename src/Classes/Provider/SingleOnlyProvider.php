<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

abstract class SingleOnlyProvider extends Provider{
    function send($message, $recipients, $from = null){
        $return = '';

        if (is_array($recipients)){
            $returns = [];
            foreach ($recipients as $recipient) {
                $returns[] = $this->sendSingleRecipient($message, $recipient, $from);
            }
            $return = implode(';', $returns);
        }else{
            $return = $this->sendSingleRecipient($message, $recipients, $from);
        }

        return $return;
    }
}