<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

class Elfo extends SingleOnlyProvider{
    const NAME = 'elfo';

    const SMS_GATEWAY_URL = 'https://sms.elfo.com/v1/sms';
    const DEFAULT_SENDER_ID = '68889';

    const CONTENT_TYPE_ASCII = 'A';
    const CONTENT_TYPE_UNICODE = 'U';

    protected $apiKey = '';
    protected $endPoint;
    protected $senderId;

    public function __construct(array $settings){
        parent::__construct($settings);

        if (isset($settings['api_key'])){
            $this->apiKey = $settings['api_key'];
        }

        if (isset($settings['endpoint'])){
            $this->endPoint = $settings['endpoint'];
        }else{
            $this->endPoint = self::SMS_GATEWAY_URL;
        }

        if (isset($settings['sender_id'])){
            $this->senderId = $settings['sender_id'];
        }else{
            $this->senderId = self::DEFAULT_SENDER_ID;
        }
    }

    public function getName(){
        return self::NAME;
    }

    function getEndPoint() {
        return $this->endPoint;
    }

    protected function getParameter($message, $recipient, $from = null) {
        return [
            'message' => $message,
            'msisdn' => $recipient,
            'senderid' => $this->senderId,
            'coding' => self::CONTENT_TYPE_ASCII,
        ];
    }

    protected function callEndPoint($endPoint, array $parameter){
        $ch = $this->prepareCurl($endPoint);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($parameter));
        curl_setopt( $ch, CURLOPT_HTTPHEADER, [
            'X-Elfo-Authentication: ' . $this->apiKey,
            'Content-Type:application/json',
        ]);

        return $this->execCurl($ch, $endPoint);
    }
}