<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

abstract class Provider{
    protected $httpEnable = false;

    abstract function send($message, $recipients, $from = null);
    abstract function getName();
    abstract function getEndPoint();
    abstract protected function getParameter($message, $recipient, $from = null);

    public function __construct(array $settings){
        if (isset($settings['http_enable']))
            $this->httpEnable = $settings['http_enable'];
    }

    protected function sendSingleRecipient($message, $recipient, $from = null){
        // parse recipient
        $recipient = $this->parseRecipient($recipient);

        // construct parameter
        $parameter = $this->getParameter($message, $recipient, $from);

        // call endpoint
        return $this->callEndPoint($this->getEndPoint(), $parameter);
    }

    /**
     * default call endpoint
     * @param $endPoint
     * @param array $parameter
     */
    protected function callEndPoint($endPoint, array $parameter){
        return $this->httpGet($endPoint, $parameter);
    }

    protected function parseRecipient($recipient){
        // remove -
        $recipient = str_replace('-', '', $recipient);

        // remove space
        $recipient = str_replace(' ', '', $recipient);

        return $recipient;
    }

    protected function httpGet($endPoint, array $parameter){
        // construct $url
        $url = $endPoint . '?' . http_build_query($parameter);

        $ch = $this->prepareCurl($url);

        return $this->execCurl($ch, $url);
    }

    protected function prepareCurl($url){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        return $ch;
    }

    protected function execCurl($ch, $url){
        // http call only if enabled
        if ($this->httpEnable){
            return curl_exec($ch);
        }else{
            return $url;
        }
    }
}