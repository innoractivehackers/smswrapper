<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

class MacroKiosk extends SingleOnlyProvider{
    const NAME = 'macrokiosk';

    const SMS_GATEWAY_URL = 'https://www.etracker.cc/bulksms/mesapi.aspx';
    const CHAR_TYPE = 'ASCII';
    const SERVICE_ID = 'MES01';
    const DEFAULT_FROM = 'RM0';

    protected $endPoint;
    protected $user;
    protected $pass;

    public function __construct(array $settings){
        parent::__construct($settings);

        $this->user = $settings['user'];
        $this->pass = $settings['pass'];

        if (isset($settings['endpoint'])){
            $this->endPoint = $settings['endpoint'];
        }else{
            $this->endPoint = self::SMS_GATEWAY_URL;
        }
    }

    public function getName(){
        return self::NAME;
    }

    function getEndPoint() {
        return $this->endPoint;
    }

    protected function getParameter($message, $recipient, $from = null) {
        if (empty($from)){
            $from = self::DEFAULT_FROM;
        }

        return [
            'user' => $this->user,
            'pass' => $this->pass,
            'type' => self::CHAR_TYPE,
            'to' => $recipient,
            'from' => $from,
            'text' => $message,
            'servid' => self::SERVICE_ID
        ];
    }
}