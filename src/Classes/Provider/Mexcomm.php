<?php

namespace Innoractive\SMSWrapper\Classes\Provider;

class Mexcomm extends Provider{
    const NAME = 'mexcomm';

    const SMS_GATEWAY_URL = 'http://bulk.ezlynx.net:7001/BULK/BULKMT.aspx';

    const SMS_TYPE_TEXT = 'TEXT';

    protected $user;
    protected $pass;

    public function __construct(array $settings){
        parent::__construct($settings);

        $this->user = $settings['user'];
        $this->pass = $settings['pass'];
    }

    public function getName(){
        return self::NAME;
    }

    function getEndPoint() {
        return self::SMS_GATEWAY_URL;
    }

    function send($message, $recipients, $from = null){
        if (is_array($recipients)){
            $recipients = implode(';', $recipients);
        }

        return $this->sendSingleRecipient($message, $recipients, $from);
    }

    protected function getParameter($message, $recipient, $from = null) {
        return [
            'user' => $this->user,
            'pass' => $this->pass,
            'msisdn' => $recipient,
            'smstype' => self::SMS_TYPE_TEXT,
            'body' => $message
        ];
    }
}