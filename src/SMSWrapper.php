<?php

/**
 * for use with laravel
 */

namespace Innoractive\SMSWrapper;

use Innoractive\SMSWrapper\Classes\Response;

class SMSWrapper {
    /**
     * @return SMSCore
     */
    protected static function getSMSCore(){
        return new SMSCore(config('sms-wrapper.providers'), config('sms-wrapper.settings'));
    }

    public static function test(){
        return 'test';
    }

    public static function send($message, $recipients, $from = null, $retryCount = 0){
        $core = self::getSMSCore();

        $response = $core->send($message, $recipients, $from, $retryCount);

        // write to log
        self::addLog($message, $recipients, $response, $retryCount);
    }

    protected static function addLog($message, $recipients, Response $response, $retryCount){
        $date = date('Y-m-d');

        // convert $recipients into string
        if (is_array($recipients)){
            $recipients = implode(';', $recipients);
        }

        // get response string
        $responseString = $response->getReturn();

        // TODO - future, validate response

        // make it into 1 line
        $responseString = str_replace(array("\r","\n"),"",$responseString);

        $log = '[' . $date . ' ' . date('H:i:s') . '] count:' . $retryCount . ' - ' .
            $response->getProvider()->getName() . ' sent :' . $responseString .
            ' {' . $recipients . '}' .
            "\n";

        $filename = config('sms-wrapper.log.file') . '-' . $date . '.log';

        file_put_contents(storage_path(config('sms-wrapper.log.dir') . '/' . $filename), $log, FILE_APPEND);
    }
}